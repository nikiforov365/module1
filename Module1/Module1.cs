using System;
using System.Linq;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 module = new Module1();
            Console.WriteLine(module.GetMinimumValue(new int[] { 8, -4, 10, -1 }));
            int[] result = module.SwapItems(3, 5);
            Console.WriteLine(result[0] + " " + result[1]);
        }
                public int[] SwapItems(int a, int b)
        {
            return new int[] { b, a };
        }

        public int GetMinimumValue(int[] input)
        {
            int min = input[0];
            for (int i = 1; i < input.Length; i++)
            {
                if (input[i] < min)
                {
                    min = input[i];
                }
            }
            return min;


        }
    }
}

